# BathroomFan

## Parts
* Adruino Nano
* 5V to 12 V DC-DC Stepdown converter (MT3608)

## Links
[Button Wiring](https://www.arduino.cc/en/tutorial/button)

[Chrono Time/Stopwatch library](https://github.com/SofaPirate/Chrono)

[Add library .zip file to Adruino](https://www.arduino.cc/en/guide/libraries)


# Behavior
System has 2 pushbuttons to add 1 and 10 minutes to the *fan run timer*

# Notes

12/20/19 Created Fritzing diagram with PCB gerber files. Ordered PCBs from [jlcpcb](https://jlcpcb.com/)

Removed gas sensor. It requred too big of a reading to trigger fan
(Gas readings are run through a filter to normalize sporatic readings. Gas readings will cause 1 minute to be added to *fan run timer* with it exceeds a hard-coded value) * MQ-5 Gas Sensor

Removed Temp/Humidity sensor. 
[DHT 11 Temp/RH Sensor](https://create.arduino.cc/projecthub/Arca_Ege/using-dht11-b0f365)  [DHT-Sensor-Library](https://github.com/adafruit/DHT-sensor-library)

Removed display. Not needed and did nto stand up to humidity. * SSD1306 32 x 129 monochrome display