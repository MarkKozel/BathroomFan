#include <SPI.h>
#include <Wire.h>

#include <Chrono.h>

// Elements for Add Minute push button
const int OneMin_BUTTON_Pin = 5;
int oneButtonState;
int oneLastState = LOW;
unsigned long oneLastDbTime = 0;
unsigned long oneDbDelay = 25;

// Elements for Add 10 Minutes push button
const int TenMin_BUTTON_Pin = 11;
int tenButtonState;
int tenLastState = LOW;
unsigned long tenLastDbTime = 0;
unsigned long tenDbDelay = 25;

const int FAN_Pin = 3; // Fan control index
int fanCurrentState = 0;
const int MAX_FAN_DC = 79;

const int MINUTE_COUNT_MAX = 30;

const int RH_FAN_ON = 50;
const int RH_FAN_OFF = 110;

//Metro Timers
Chrono timerKeyPressed;
Chrono timerLED;
Chrono timerOneMinute(Chrono::SECONDS);

int minutesLeftOnFan = 0;

void setup()
{
  Serial.begin(115200);

  // Setup fan and init PWM on
  pinMode(FAN_Pin, OUTPUT);
  pwm25kHzBegin();
  pwmDuty(0);

  pinMode(OneMin_BUTTON_Pin, INPUT);
  pinMode(TenMin_BUTTON_Pin, INPUT);
}

void loop()
{
  //Check for end of fan run time (minutesLeftOnFan). Decrement and
  //turn off fan if time has ended
  if (timerOneMinute.hasPassed(60))
  {
    timerOneMinute.restart();
    if (minutesLeftOnFan > 0)
    {
      minutesLeftOnFan--;

      if ((minutesLeftOnFan == 0) && (fanCurrentState != 0))
      { //Time's up, turn off fan
        fanCurrentState = 0;
        pwmDuty(fanCurrentState); // Turn off fan
        Serial.print("Fan Off");
      }
      else
      {
        Serial.print("Time left : ");
        Serial.println(minutesLeftOnFan);
      }
    }
  }

  if (timerKeyPressed.hasPassed(100))
  {
    timerKeyPressed.restart();

    //1 minute button
    int oneRead = digitalRead(OneMin_BUTTON_Pin);
    if (oneRead != oneLastState)
    {
      oneLastDbTime = millis();
    }

    if ((millis() - oneLastDbTime) > oneDbDelay)
    {
      if (oneRead != oneButtonState)
      {
        oneButtonState = oneRead;
        if (oneButtonState == HIGH)
        {
          addToFanTime(1);
        }
      }
    }
    oneLastState = oneRead;

    //10 minute button
    int tenRead = digitalRead(TenMin_BUTTON_Pin);
    if (tenRead != tenLastState)
    {
      tenLastDbTime = millis();
    }

    if ((millis() - tenLastDbTime) > tenDbDelay)
    {
      if (tenRead != tenButtonState)
      {
        tenButtonState = tenRead;
        if (tenButtonState == HIGH)
        {
          addToFanTime(10);
        }
      }
    }
    tenLastState = tenRead;
  }
}

// Called when button is pressed. Adds 1 minute unless already at
// MINUTE_COUNT_MAX
void addToFanTime(int timeToAdd)
{
  if (minutesLeftOnFan < MINUTE_COUNT_MAX)
  {
    // countdownSeconds = countdownSeconds + 60; // convert to seconds
    minutesLeftOnFan += timeToAdd;

    Serial.print("New time : ");
    Serial.println(minutesLeftOnFan);

    if (minutesLeftOnFan > MINUTE_COUNT_MAX)
    {
      minutesLeftOnFan = MINUTE_COUNT_MAX;
    }
    fanCurrentState = MAX_FAN_DC;
    pwmDuty(fanCurrentState); // Turn on fan
  }
}

// Setup TC2 for PWM Fan Control
void pwm25kHzBegin()
{
  TCCR2A = 0; // TC2 Control Register A
  TCCR2B = 0; // TC2 Control Register B
  TIMSK2 = 0; // TC2 Interrupt Mask Register
  TIFR2 = 0;  // TC2 Interrupt Flag Register
  TCCR2A |=
      (1 << COM2B1) | (1 << WGM21) |
      (1 << WGM20);                     // OC2B cleared/set on match when up/down counting, fast PWM
  TCCR2B |= (1 << WGM22) | (1 << CS21); // prescaler 8
  OCR2A = 79;                           // TOP overflow value (Hz)
  OCR2B = 0;
}

// Set/Change PWM Fan duty cycle
void pwmDuty(byte ocrb)
{
  OCR2B = ocrb; // PWM Width (duty)
}
